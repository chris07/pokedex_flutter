
import 'package:flutter/material.dart';
import 'beer_list.dart';

void main() => runApp(const BeersApp());

class BeersApp extends StatelessWidget {
  const BeersApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Liste des bierres Brew Dog',
      theme: ThemeData(primaryColor: Colors.green.shade100),
      home: const BeerListView(),
    );
  }
}
