
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'beer.dart';

class BeerListView extends StatefulWidget {
  const BeerListView({Key? key}) : super(key: key);

  @override
  _BeerListViewState createState() => _BeerListViewState();
}

class _BeerListViewState extends State<BeerListView> {
  final _beers = <Beer>[];
  final _biggerFont = const TextStyle(fontSize: 25);
  int _pageNumber = 1;
  final int _nextPageThreshold = 15;

  @override
  void initState() {
    super.initState();
    _loadData();
  }

  Future<void> _loadData() async {
    final response = await http.get(Uri.parse('https://api.punkapi.com/v2/beers?page=$_pageNumber'));
    setState(() {
      final dataList = json.decode(response.body) as List;
      _pageNumber = _pageNumber + 1;
      for (final item in dataList) {
        final name = item['name'] as String? ?? '';
        final url = item['image_url'] as String? ?? '';
        final firstBrewed = item['first_brewed'] as String? ?? '';
        final description = item['description'] as String? ?? '';
        final abv = item['abv'].toString();
        final ph = item['ph'].toString();
        final beer = Beer(name, url, firstBrewed, description, abv, ph);
        _beers.add(beer);
      }
    });
  }

  Widget _buildRow(int i) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: ListTile(
        title: Text(_beers[i].name, style: _biggerFont),
        leading: CircleAvatar(
          backgroundColor: Colors.green,
          backgroundImage: getImage(i),
        ),
        trailing: IconButton(
          icon: const Icon(Icons.arrow_forward_ios),
          tooltip: 'Show',
            onPressed: (){
              showDetails(_beers[i]);
            } ,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Brew Dog Beers'),
      ),
      body: getBody()
    );
  }

  Widget getBody() {

    return ListView.separated(
        itemCount: _beers.length,
        itemBuilder: (BuildContext context, int position) {
          if (position == _beers.length - _nextPageThreshold) {
            _loadData();
          }
          return _buildRow(position);
        },
        separatorBuilder: (context, index) {
          return const Divider();
        });
  }

  getImage(int i) {
    if(_beers[i].image_url != ""){
      return  NetworkImage(_beers[i].image_url);
    }
    else{
      return const NetworkImage("https://cdn.pixabay.com/photo/2016/03/12/14/19/error-404-1252056_1280.png");
      }
  }

  getImgDetail(img){
    if( img != ""){
      return  img;
    }
    else{
      return "https://cdn.pixabay.com/photo/2016/03/12/14/19/error-404-1252056_1280.png";
    }
  }

  showDetails(_beer) {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
          builder: (BuildContext context) {
            final info = _beer;
            return Scaffold(
              appBar: AppBar(
                title: const Text('Details'),
              ),
              body:SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                  Container(
                    height: 400.0,
                    width: 200.0,
                    margin: const EdgeInsets.fromLTRB(0, 30, 0, 30),
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: NetworkImage(getImgDetail(info.image_url)),
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                  Center(child:Container(
                    padding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
                      child: Text(info.name,
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 40,
                        ),
                      ),
                    ),
                  ),
                  Center(child:Container(
                    padding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
                      child: Text(info.description,
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                          fontSize: 25,
                        ),
                      ),
                    ),
                  ),
                  Center(child:Container(
                    padding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
                      child: Text("Date de premier brassage : ${info.first_brewed}",
                        style: const TextStyle(
                          fontSize: 20,
                        ),
                      ),
                    ),
                  ),
                  Center(child:Container(
                    padding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
                      child: Text(" Alcool : ${info.abv} degrès",
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 25,
                        ),
                      ),
                    ),
                  ),
                  Center(child:Container(
                    padding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
                      child: Text("ph : ${info.ph}",
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 25,
                        ),
                      ),
                    ),
                  ),
                ],
              ),

              ),
            );
          }
      )
    );
  }
}
