class Beer {
  const Beer(this.name, this.image_url, this.first_brewed, this.description, this.abv, this.ph);

  final String name;
  final String image_url;
  final String first_brewed;
  final String description;
  final String abv;
  final String ph;
}
